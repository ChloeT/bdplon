<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    /**
     * setUp, relancé à chaque test
     */
    public function setUp() {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();   
    }



    public function testCategories()
    // us3 scenario1
    {
        $client = static::createClient([], [
                'PHP_AUTH_USER' => 'mail1@mail.com',
                'PHP_AUTH_PW'   => '1234'
            ]);

        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorExist('select');
        $this->assertCount(7, $crawler->filter('option'));


        $this->assertSelectorTextContains('h3', 'Selectionné pour vous !');
        $this->assertCount(3, $crawler->filter('.selection'));

    }   




    public function testCategoriesNotConnected()
    // us3 sce
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorNotExists('select');

        $this->assertSelectorTextNotContains('h3', 'Selectionné pour vous !');
        $this->assertSelectorNotExists('.selection');

    }












    // us4 :

    // Comme un client,
    // Je veux pouvoir créer une liste de favoris de bd,
    // Afin de ne pas oublier de lire plus tard des BD que j’ai consulté


    // given :
    // arrivée sur la page d'acceuil
    // connecté

    // when :
    // clic sur le bouton pour mettre en fav 

    // then :
    // liste de favoris +1
    // passage du bouton de 'ajouter' à 'enlever' des favoris (ou passage d'un coeur/etoile vide à plein)
    
    // criteres d'acceptabilité :
    // url '/' qui marche
    // user_client
    // .notFav au départ
    // $favCount = user.fav au départ
    // .myFav à la fin
    // user.fav === $favCount+1 à la fin







    // Préalables : 

    // Dans l'Entity User :
    // -avoir ajouté la propriété 'favorites' en 'relation' à l'Entity User

    // Dans les Fixtures :
    // -avoir 1 User 'mail1@mail.com' avec pass '1234'
    // -avoir 2 fav sur le User 'mail1@mail.com'

    // Dans Controller+twig :
    // -avoir une page '/fav' accessible seulement si connecté, redirection vers '/register' si ce n'est pas le cas
    // -avoir un lien vers les fav en page d'accueil '/' avec l'id '#goToFav' uniquement si connecté
    // -afficher la liste des favoris dans la page '/fav'
    // -avoir un bouton qui ajoute/enleve la bd des favoris partout où leur carte est afichée, avec une classe '.notFav' si pas dans les fav et '.myFav' si dans les fav



    /**
     * us4 scenario 1 : verifier qu'on a un lien vers la page '/fav' dans la page '/' portant l'id '#goToFav' si connecté
     * 
     */
    public function testGoToFavLink()

    {
        //GIVEN
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'mail1@mail.com',
            'PHP_AUTH_PW'   => '1234'
        ]);


        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();

        //WHEN
        $link = $crawler
            ->filter('#goToFav')
            ->eq(0)
            ->link();

        $crawler = $client->click($link);

        //THEN
        $this->assertResponseRedirects('/fav');

        // $crawler = $client->request('GET', '/fav');
        $this->assertResponseIsSuccessful();

        $this->assertCount(3, $crawler->filter(".myFav"));

        
    }




    /**
     * us4 scenario 2 : verifier qu'on n'a pas le lien vers '/fav' si on n'est pas connecté
     * 
     */
    public function testNoGoToFavLinkIfNotMember()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorNotExists('#goToFav');
    }




    /**
     * us4 scenario 3 : verifier qu'on est redirigé vers la page /register si on essai d'acceder direct à /fav sans etre connecté
     * 
     */
    public function testAccesMemberOnly()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/fav');
        $this->assertResponseRedirects('/register');
    }



    /**
     * us4 scenario 4 : verifier que la page fav affiche les 2 fav de l'utilisateur 'mail1@mail.com'
     */
    public function testSeeFav()
    
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'mail1@mail.com',
            'PHP_AUTH_PW'   => '1234'
        ]);

        $crawler = $client->request('GET', '/fav');
        $this->assertResponseIsSuccessful();

        // pas vraiment utile comme on verrif un truc avec juste après, mais je ne suis pas sure de moi dans la rédaction du test
        $this->assertSelectorExist('.myFav');

        $this->assertCount(2, $crawler->filter('.myFav'));
    }




    /**
     * us4 scenario 5 : verrifier qu'on ajoute bien un fav
     * 
     */
    public function testAddToFav()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'mail1@mail.com',
            'PHP_AUTH_PW'   => '1234'
        ]);

        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();

        $link = $crawler
            ->filter('.notFav')
            ->eq(0)
            ->link();

        $crawler = $client->click($link);


        $crawler = $client->request('GET', '/fav');
        $this->assertResponseIsSuccessful();

        $this->assertCount(3, $crawler->filter(".myFav"));
    }




    /**
     * us4 scenario 6 : verrifier qu'on suprime bien un fav  
     * 
     */ 
    public function testDeleteFromFav()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'mail1@mail.com',
            'PHP_AUTH_PW'   => '1234'
        ]);

        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();

        $link = $crawler
            ->filter('.myFav')
            ->eq(0)
            ->link();

        $crawler = $client->click($link);


        $crawler = $client->request('GET', '/fav');
        $this->assertResponseIsSuccessful();

        $this->assertCount(1, $crawler->filter(".myFav"));
    }
}
