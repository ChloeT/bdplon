<?php

namespace App\DataFixtures;

use App\Entity\Bd;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // On configure dans quelles langues nous voulons nos données
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 10; $i++) {
            $bd = new Bd();
            $bd->setTitle($faker->sentence(3));
            $bd->setAuthor($faker->name);
            $bd->setDescription($faker->paragraph(4));
            $bd->setParution($faker->dateTime());
            
            $manager->persist($bd);
        }




        $manager->flush();
    }
}
