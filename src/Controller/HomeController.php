<?php

namespace App\Controller;

use App\Repository\BdRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="landing")
     */
    public function index(BdRepository $repo)
    {
        $bds = $repo-> findAll();
        $selection = [];

        for($i=0;$i<4; $i++){
            $bd=$bds[rand(0, count($bds)-1)];
            $selection[]=$bd;
        }


        
        
        return $this->render('home/index.html.twig', [
            'selection' => $selection,
        ]);
    }
}

